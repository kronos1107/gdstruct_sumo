#include <iostream>
#include <string>
#include "UnorderedArray.h"
#include "OrderedArray.h"
#include <time.h>

using namespace std;

void main()
{
	srand(time(NULL));

	cout << "Enter size of array: ";
	int size;
	cin >> size;

	UnorderedArray<int> unordered(size);
	OrderedArray<int> ordered(size);

	for (int i = 0; i < size; i++)
	{
		int rng = rand() % 101;
		unordered.push(rng);
		ordered.push(rng);
	}

	system("pause");
	system("cls");

	bool isOperating = true;

	while (isOperating)
	{
		cout << "Unordered: ";
		for (int i = 0; i < unordered.getSize(); i++)
			cout << unordered[i] << "   ";

		cout << "\nOrdered: ";
		for (int i = 0; i < ordered.getSize(); i++)
			cout << ordered[i] << "   ";


		cout << "\n\n=========================" << endl;
		cout << "Take your pick: " << endl;
		cout << "[1] - Remove an element" << endl;
		cout << "[2] - Search an element" << endl;
		cout << "[3] - Expand and generate random values" << endl;
		cout << "[4] - Exit \n" << endl;

		int choice;

		cout << "Input: " << endl;
		cin >> choice;

		int value;

		if (choice == 1)
		{
			cout << "Which index do you want to remove?" << endl;
			cout << "Input: " << endl;
			cin >> value;

			unordered.remove(value);
			ordered.remove(value);

			cout << "Here's an Updated list of your Arrays:" << endl;
			cout << "Unordered: ";
			for (int i = 0; i < unordered.getSize(); i++)
				cout << unordered[i] << "   ";

			cout << "\nOrdered: ";
			for (int i = 0; i < ordered.getSize(); i++)
				cout << ordered[i] << "   ";

			cout << endl;
			system("pause");
		}
		else if (choice == 2)
		{
			cout << "Which element do you want to search?" << endl;
			cout << "Input: ";
			cin >> value;

			cout << "Unordered Array(Linear Search):\n";
			int result = unordered.linearSearch(value);
			if (result >= 0)
				cout << value << " was found at index " << result << ".\n";
			else
				cout << value << " not found." << endl;

			cout << "Ordered Array(Binary Search):\n";
			result = ordered.binarySearch(value);
			if (result >= 0)
				cout << value << " was found at index " << result << ".\n";
			else
				cout << value << " not found." << endl;

			system("pause");
		}
		else if (choice == 3)
		{
			cout << "How many more numbers do you want to add?" << endl;
			cout << "Input: ";
			cin >> value;

			for (int i = 0; i < value; i++)
			{
				int rng = rand() % 101;
				unordered.push(rng);
				ordered.push(rng);
			}

			cout << "\nArray expaded. Here's an Updated list of your Arrays:" << endl;

			cout << "Unordered: ";
			for (int i = 0; i < unordered.getSize(); i++)
				cout << unordered[i] << "   ";

			cout << "\nOrdered: ";
			for (int i = 0; i < ordered.getSize(); i++)
				cout << ordered[i] << "   ";

			cout << endl;
			system("pause");
		}
		else if (choice == 4)
		{
			isOperating = false;
		}

		system("cls");
	}
}