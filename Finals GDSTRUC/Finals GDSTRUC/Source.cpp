#include <iostream>
#include "Stack.h"
#include "Queue.h"
#include <string>

using namespace std;

void main()
{
	int size;
	int choice;
	bool isInUse = true;
	int pushInput;

	cout << "Enter array size: ";
	cin >> size;
	cout << endl;

	system("pause");
	system("cls");

	Stack<int> stack(size);
	Queue<int> queue(size);

	while (isInUse)
	{
		cout << "Choose among: " << endl;
		cout << "[1] - push" << endl;
		cout << "[2] - pop" << endl;
		cout << "[3] - print" << endl;
		cout << "[4] - exit" << endl;
		cin >> choice;

		switch (choice)
		{
		case 1:
			cout << "Input number: ";
			cin >> pushInput;

			stack.push(pushInput);
			queue.push(pushInput);

			cout << "Stack: " << pushInput << endl;
			cout << "Queue: " << queue.top() << endl;

			system("pause");
			system("cls");
			break;

		case 2:
			stack.pop();
			queue.pop();

			system("pause");
			system("cls");
			break;
		case 3:
			cout << "Stack: ";

			for (int i = 0; i < stack.getSize(); i++)
				cout << stack[i] << ", ";

			cout << "\nQueue: ";

			for (int i = 0; i < queue.getSize(); i++)
				cout << queue[i] << ", ";

			for (int i = 0; i < stack.getSize(); i++)
				stack.pop();

			for (int i = 0; i < queue.getSize(); i++)
				queue.pop();

			system("pause");
			system("cls");
			break;
		case 4:
			isInUse = false;
		default:
			break;
		}
	}
};