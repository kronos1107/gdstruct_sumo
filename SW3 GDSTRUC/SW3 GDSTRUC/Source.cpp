#include<iostream>
#include <conio.h>
#include<string>

using namespace std;

void showMembers(string *memName, int memSize)
{
	cout << "Member List: \n" << endl;

		for (int i = 0; i < memSize; i++)
		{
			cout << i + 1 << ".) " << memName[i] << endl;
		}
}

void renameMember(string *memName, int memSize)
{
	string choice;

	cout << "Select which member you want to rename from the list: " << endl;

	showMembers(memName, memSize);

	cout << "\n Input member's name: ";
	cin >> choice;

	for (int i = 0; i < memSize; i++)
	{
		if (choice == memName[i])
			cout << memName[i] << " will be renamed" << endl;
			cout << "Input Name: ";
			cin >> memName[i];
	}
}

void addMember(string *memName, int memSize)
{
	int additional;

	cout << "How many members do you want to add? " << endl;
	cin >> additional;

	memName = new string[memSize + additional];

	for (int i = 0; i < memSize; i++)
	{
		cout << "Member's name: ";
		cin >> memName[i];
		cout << endl;
	}
}

void deleteMember(string *memName, int memSize)
{
	string choice;

	showMembers(memName, memSize);

	cout << "Who do you want to remove? " << endl;
	cin >> choice;

	for (int i = 0; i < memSize; i++)
	{
		if (choice == memName[i])
		{
			for (int j = i; j < (memSize - 1); j++)
			{
				memName[j] = memName[j + 1];
			}
		}
	}
}

int main()
{
	string guildName;
	int memSize;
	int choice;

	cout << "Good day!! wht do you want to name your guild? ";
	cin >> guildName;

	system("pause");

	cout << "How many members do you want? ";
	cin >> memSize;

	string *memName = new string[memSize];

	for (int i = 0; i < memSize; i++)
	{
		cout << "Member's name: ";
		cin >> memName[i];
		cout << endl;
	}

	system("pause");
	system("cls");

	cout << "What do you want to do? \n" << endl;
	cout << "[1] - Show Members" << endl;
	cout << "[2] - Rename Member" << endl;
	cout << "[3] - Add New Member" << endl;
	cout << "[4] - Delete Memeber" << endl;
	cout << "[5] - Exit" << endl;

	cin >> choice;

	switch (choice)
	{
	case 1:
		showMembers(memName, memSize);
		break;
	case 2:
		renameMember(memName, memSize);
		break;
	case 3:
		addMember(memName, memSize);
		break;
	case 4:
		deleteMember(memName, memSize);
	case 5:
		return 0;
	}

	_getch();
	return 0;
}