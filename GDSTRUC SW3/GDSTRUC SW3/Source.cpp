#include <iostream>

using namespace std;

//adding input numbers
int add(int value)
{
	if (value < 10)
		return value;

	return((value % 10) + add(value / 10));
}

//fibonacci sequence
int fibonacci(int value, int firstDigit, int secondDigit)
{
	cout << firstDigit << ", ";

	if (value == 0)
		return firstDigit + secondDigit;

	return(fibonacci(value - 1, secondDigit, firstDigit + secondDigit));
}
 
//determine whether the value is a prime number or not
int primeCheck(int value, int n)
{
	if (n == 1)
		return 1;
	else if (value % n == 0)
		return 0;

	return primeCheck(value, n - 1);

}

void main()
{
	int value;
	int firstDigit = 0;
	int secondDigit = 1;

	cout << "Input a value: ";
	cin >> value;

	cout << "Sum of values: " << add(value) << endl;
	cout << "Fibonacci Sequence: ";
	cout << fibonacci(value, firstDigit, secondDigit) << endl;
	cout << "Is it a Prime number? ";

	if (primeCheck(value, value / 2) == 1)
		cout << "It is a Prime number" << endl;
	else
		cout << "It is not a Prime number" << endl;

	system("pause");
} 