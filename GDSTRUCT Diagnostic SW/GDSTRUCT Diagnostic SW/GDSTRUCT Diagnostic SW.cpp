// GDSTRUCT Diagnostic SW.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <conio.h>
#include <string>
#include <time.h>

using namespace std;

//Bubble Sort lowest to highest
void ascending(int numbers[10])
{
	for (int i = 0; i < 9; i++)
	{

		for (int j = i + 1; j < 10; j++)
		{
			int temp = numbers[i];

			if (temp > numbers[j])
			{
				numbers[i] = numbers[j];
				numbers[j] = temp;
			}
		}
	}

	for (int i = 0; i < 10; i++)
	{
		cout << numbers[i] << ", " << endl;
	}

	system("pause");
}

//Bubble Sort highest to lowest
void descending(int numbers[10])
{

	for (int i = 0; i < 9; i++)
	{

		for (int j = i + 1; j < 10; j++)
		{
			int temp = numbers[i];

			if (temp < numbers[j])
			{
				numbers[i] = numbers[j];
				numbers[j] = temp;
			}
		}
	}

	for (int i = 0; i < 10; i++)
	{
		cout << numbers[i] << ", " << endl;
	}

	system("pause");
}

int linearSearch(int numbers[10], int value)
{
	int counter = 0;

	for (int i = 1; i < 10; i++)
	{
		counter++;

		if (value == numbers[i])
		{
			cout << "It took " << counter << " times." << endl;
			return i;
		}

	}

	return -1;
}

int main()
{
	srand(time(NULL));
	int numbers[10];
	int temp;
	char choice;
	int value;

	//Random numbers between 1-69
	for (int i = 0; i < 10; i++)
	{
		numbers[i] = rand() % 69 + 1;
		cout << numbers[i] << endl;
	}

	system("pause");

	cout << "\nWould you like it to be arranged in ASCENDING or DESCENDING order?" << endl;
	cout << "[1] - ASCENDING\n[2] - DESCENDING" << endl;

	//User's input between Ascending to Descending order
	cin >> choice;
	cout << endl;

	switch (choice)
	{
	case '1':
		ascending(numbers);
		break;
	case '2':
		descending(numbers);
		break;
	}

	cout << "Provide a value to perform a LINEAR SEARCH: ";
	cin >> value;

	int location = linearSearch(numbers, value);

	if (location > 0)
	{
		cout << value << " is found at " << location;
	}
	else if (location < 0)
	{
		cout << value << " NOT FOUND.";
	}

	_getch();
    return 0;
}

