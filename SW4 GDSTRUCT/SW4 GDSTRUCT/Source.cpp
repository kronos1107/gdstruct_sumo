#include <iostream>
#include <string>
#include <vector>
#include <time.h>
#include "UnorderedArray.h"

using namespace std;

void main()
{
	srand(time(NULL));

	cout << "Enter size of array: ";
	int size;
	cin >> size;

	UnorderedArray<int> unordered(size);

	for (int i = 0; i < size; i++)
	{
		int rng = rand() % 101;
		unordered.push(rng);
	}

	system("pause");
	system("cls");

	bool isOperating = true;

	while (isOperating)
	{
		cout << "Unordered: ";
		for (int i = 0; i < unordered.getSize(); i++)
			cout << unordered[i] << "   ";

		cout << "\n\n=========================" << endl;
		cout << "Take your pick: " << endl;
		cout << "[1] - Remove an element" << endl;
		cout << "[2] - Exit \n" << endl;

		int choice;

		cout << "Input: " << endl;
		cin >> choice;

		int value;

		if (choice == 1)
		{
			cout << "Which index do you want to remove?" << endl;
			cout << "Input: " << endl;
			cin >> value;

			unordered.remove(value);

			cout << "Here's an Updated list of your Arrays:" << endl;
			cout << "\nUnordered: ";
			for (int i = 0; i < unordered.getSize(); i++)
				cout << unordered[i] << "   ";

			cout << endl;
			system("pause");
		}
		else if (choice == 2)
		{
			isOperating = false;
		}

		system("cls");
	}

}
	
	